// Created in 2018 by Norman Shamas
// License CC BY-NC-SA: https://creativecommons.org/licenses/by-nc-sa/4.0/

package main

import (
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"flag"
	"fmt"
	"hash"
	"io"
	"os"
)

// Three global variables are defined:
// alg defines the hashing algorithm that will be used and defaults to sha256
// file is true/false flag to indicate whether the user input is a filepath
// h is a placeholder for the hashing algorithm that is assigned after error checks
var alg int
var file bool
var h hash.Hash

const (
	SHA1       int = 1
	SHA224     int = 224
	SHA256     int = 256
	SHA384     int = 384
	SHA512     int = 512
	SHA512_224 int = 512224
	SHA512_256 int = 512256
)

// Init reads the flags and assigns values to the alg and file variables.
func init() {
	flag.IntVar(&alg, "a", SHA256, "Please select the hashing algorithm: 1 (deprecated), 224, 256, 384, "+
		"512, 512224, 512256 (shorthand flag)")
	flag.IntVar(&alg, "algo", SHA256, "Please select the hashing algorithm: 1 (deprecated), 224, 256, "+
		"384, 512, 512224, 512256")
	flag.BoolVar(&file, "f", false, "Indicate whether the input is a file path instead of a string")
}

// ErrorChecks checks whether the user has input valid choices for the hashing algorithms
// and makes sure only one string or file is provided
func errorChecks() {
	algoChoices := map[int]bool{SHA1: true, SHA224: true, SHA256: true, SHA384: true, SHA512: true,
		SHA512_224: true, SHA512_256: true}
	if _, validChoice := algoChoices[alg]; !validChoice {
		flag.PrintDefaults()
		os.Exit(1)
	}

	if len(flag.Args()) > 1 {
		fmt.Println("You can only send one string or file to be hashed. If you would like to have multiple words," +
			" surround by \". For example \"test text\"")
		os.Exit(1)
	}
}

// SumFile is run only when the file flag is true. It reads the file and creates a hash of the
// file based on the selected algorithm. No variables are input or returned since it uses the
// global variables.
func sumFile() {
	f, err := os.Open(flag.Args()[0])
	if err != nil {
		panic(err)
	}
	defer f.Close()

	if _, err := io.Copy(h, f); err != nil {
		panic(err)
	}
}

func main() {
	flag.Parse()
	errorChecks()

	// This switch assigns h to the valid hashing algorithm.
	switch alg {
	case SHA1:
		h = sha1.New()
	case SHA224:
		h = sha256.New224()
	case SHA256:
		h = sha256.New()
	case SHA384:
		h = sha512.New384()
	case SHA512:
		h = sha512.New()
	case SHA512_224:
		h = sha512.New512_224()
	case SHA512_256:
		h = sha512.New512_256()
	}

	if file == true {
		sumFile()
	} else {
		h.Write([]byte(flag.Args()[0]))
	}

	fmt.Printf("%s:\t%x\n", flag.Args()[0], h.Sum(nil))
}
