# go-shasum

This was a quick rewrite of shasum into Go as a way to learn basics of Go

## Usage
`go build shasum.go`  
`./shasum *text* or ./shasum -f *filepath*`  
You can use the `-h` flag to see the different algorithm options